import serial
import csv
import time
import threading

Port = 'COM20'
Baudrate = 38400
ser = serial.Serial(port = Port,
    baudrate = Baudrate,
    bytesize = serial.EIGHTBITS,
    timeout =None,
    parity=serial.PARITY_NONE)
# Voltage set
Vref = 5.0
Resolution = 65535
V_bit = Vref / Resolution
Data_size = 8400


def Read_serial(ser):
    print(ser.isOpen)
    print(ser.port)
    while True:
        while ser.inWaiting():
            for i in range(Data_size):
                data_raw = ser.readline()
                data_temp = int(data_raw.decode('utf-8'),16) * V_bit
                with open("first_data.csv","a") as f:
                    writer = csv.writer(f,delimiter=",")
                    writer.writerow([data_temp])
            pass
            ser.close()
        # pass
    # ser.close()

thread = threading.Thread(target=Read_serial,args=(ser,))
thread.start()
# if __name__ == "__main__":
#     thread = threading.Thread(target=Read_serial,args=(ser,))
#     try:
#         thread.start()
#     except KeyboardInterrupt:
#         ser.close()