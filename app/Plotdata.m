clc; clear all;close all;
[voltage] = textread(['D:\project\python\Serial\app\pulse_data.csv'],'%f');
[voltage2] = textread(['D:\project\python\Serial\app\10ucurrent_data.csv'],'%f');
R1 =1;
R2 =1;
R3 =18;
R4 =12;
Vref = 4.8;
Resolution = 65535;
V_bit = Vref / Resolution;
% 20u
% Rtest = 56*10^4;
% 100u
% Rtest = 10 * 10^4;
% 10u
Rtest = 10*10^5
Rlp = 1* 10^3;
Rg = 52*10^3;
G = 1+(49.4*10^3/Rg);
Vtest = 5;


for i = 1: length(voltage)
        voltage(i) =((-5*R1*R3)-(5*R1*R4)+(R1*R3*voltage(i))+(R2*R3*voltage(i)))/(R2*(R3+R4));
end

for i = 1: length(voltage2)
        voltage2(i) =((-5*R1*R3)-(5*R1*R4)+(R1*R3*voltage2(i))+(R2*R3*voltage2(i)))/(R2*(R3+R4));
end

% ideal current
for i = 1: length(voltage)
        i_current(i) = (Vtest - voltage(i))/Rtest;
end

% real current
M = movmean(voltage2,10);
% for i = 1: length(voltage2)
%         r_current(i) = voltage2(i)/Rlp/G/2;
% end
for i = 1: length(M)
        r_current(i) = M(i)/Rlp/G;
end

% ideal current
for i = 1: length(i_current)
    i_error(i) = (abs(i_current(i)) - abs(r_current(i)));
end

T = 15e-6;


x1 = [0:T:T*2600];
x2 = [0:T:T*3600];
% mov_v = movmean(voltage,5);
% 0414
f1 = figure;
plot(x1,voltage(5000:7600));
title('激發脈衝')
xlabel('Second') 
ylabel('Voltage') 
f2 = figure;
% plot(r_current(3000:7600));
plot(x1,voltage2(5000:7600));
title('電流激發')
xlabel('Second') 
ylabel('Ampere') 
f3 = figure;
plot(x1,i_current(5000:7600));
title('電流激發')
xlabel('Second') 
ylabel('Ampere') 
hold on
plot(x1,r_current(5000:7600));
legend({'理想電流','實測電流'})
f5 = figure;
plot(x1,i_error(5000:7600));
title('量測誤差')
xlabel('Second') 
ylabel('Ampere')
f6 = figure;
plot(x1,i_current(5000:7600));
title('理想電流激發')
xlabel('Second') 
ylabel('Ampere') 

